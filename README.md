# DOCLINE TEST

[![Website docline.mikelgoig.com](https://img.shields.io/website?down_color=red&down_message=down&up_color=green&up_message=up&url=https%3A%2F%2Fdocline.mikelgoig.com)](https://docline.mikelgoig.com)

## Demo

👉 https://docline.mikelgoig.com

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- [Docker](https://www.docker.com) >= 20.10
- [Yarn](https://yarnpkg.com) >= 1.22

### Installing

1. Clone the repository from GitLab:

   ```bash
   git clone git@gitlab.com:mikelgoig/docline-test.git && cd docline-test
   ```

2. Open Docker app.

3. Build the project:

   ```bash
   .cli/build.sh
   ```

4. Run the project:

   ```bash
   .cli/up.sh
   ```

5. Stop the project:

   ```bash
   .cli/down.sh
   ```

## Running the analysis tools

- [PHP Coding Standards Fixer (PHP-CS-Fixer)](https://cs.symfony.com) - A tool to automatically fix PHP Coding Standards issues
- [Larastan](https://github.com/nunomaduro/larastan) - Adds static analysis to Laravel improving developer productivity and code quality
- [PHP Copy/Paste Detector (PHP-CPD)](https://github.com/sebastianbergmann/phpcpd) - Copy/Paste Detector (CPD) for PHP code

To run all the PHP analysis tools:

```bash
composer analyse
```

## Running the tests

To run all the PHPUnit tests:

```bash
composer test
```

## Deployment

We have set up **continuous deployment** with Laravel Forge. Every time we push to the **master** branch, we deploy the new changes to our **production** environment.

## Built With

- [Laravel](https://laravel.com) - The PHP Framework For Web Artisans
- [Vue.js](https://vuejs.org) - The Progressive JavaScript Framework
- [Tailwind CSS](https://tailwindcss.com) - A utility-first CSS framework for rapid UI development

## Authors

- **Mikel Goig** - _Frontend and backend development_ - [mikelgoig](https://github.com/mikelgoig)
