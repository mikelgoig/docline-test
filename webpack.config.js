const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        exclude: [/node_modules/, /vendor/],
        loader: 'eslint-loader',
        options: {
          fix: true,
          cache: false,
        },
      },
    ],
  },

  output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },

  resolve: {
    alias: {
      vue$: 'vue/dist/vue.runtime.esm.js',
      '@': path.resolve('resources/js'),
    },
  },
};
