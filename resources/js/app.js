/*
|------------------------------------------------------------------------------
| Project Dependencies
|------------------------------------------------------------------------------
|
| First we will load all of this project's JavaScript dependencies which
| includes Vue and other libraries. It is a great starting point when
| building robust, powerful web applications using Vue and Laravel.
|
*/

/* Vue */
import Vue from 'vue';

/* Vendor */
import {
  App as InertiaApp,
  plugin as InertiaPlugin,
} from '@inertiajs/inertia-vue';

/* Helpers */
import mixin from '@/Helpers/vue-mixin';

/* Plugins */
import InertiaProgress from '@/Plugins/inertia-progress';
import VueAutofocus from '@/Plugins/vue-autofocus';
import VueClickOutside from '@/Plugins/vue-click-outside';
import VueLodash from '@/Plugins/vue-lodash';
import VueNumberFormat from '@/Plugins/vue-number-format';
import VuePortal from '@/Plugins/vue-portal';

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/*
|------------------------------------------------------------------------------
| Vue Setup
|------------------------------------------------------------------------------
|
| Next, we will setup the Vue application.
|
*/

if (process.env.MIX_APP_ENV === 'production') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
  Vue.config.productionTip = false;
}

/* Vue Helpers */
Vue.mixin(mixin);

/* Vue Plugins */
Vue.use(InertiaPlugin);
Vue.use(InertiaProgress);
Vue.use(VueAutofocus);
Vue.use(VueClickOutside);
Vue.use(VueLodash);
Vue.use(VueNumberFormat);
Vue.use(VuePortal);

/**
 * The following block of code may be used to automatically register your
 * shared Vue components. It will recursively scan the Vue components directory
 * and automatically register them with their "basename".
 */

const components = require.context('./Shared/', true, /\.vue$/i);
components.keys().map(key =>
  Vue.component(
    key
      .split('/')
      .pop()
      .split('.')[0],
    components(key).default
  )
);

/*
|------------------------------------------------------------------------------
| Vue Application
|------------------------------------------------------------------------------
|
| Finally, we will create a fresh Vue application instance and attach it to
| the main page.
|
*/

const app = document.getElementById('app');

window.DoclineTestApp = new Vue({
  render: h =>
    h(InertiaApp, {
      props: {
        initialPage: JSON.parse(app.dataset.page),

        resolveComponent: name =>
          import(`./Pages/${name}`).then(module => module.default),
      },
    }),
}).$mount(app);
