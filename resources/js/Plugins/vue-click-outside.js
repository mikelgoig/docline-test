import VClickOutside from 'v-click-outside';

export default {
  install(Vue) {
    Vue.use(VClickOutside);
  },
};
