import VueLodash from 'vue-lodash';

import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';

export default {
  install(Vue) {
    Vue.use(VueLodash, {
      name: '$lodash',
      lodash: {
        get,
        mapValues,
        pickBy,
      },
    });
  },
};
