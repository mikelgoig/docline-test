export default {
  install(Vue) {
    Vue.directive('autofocus', {
      inserted(el, { value }) {
        if (value) {
          el.focus();
        }
      },
    });
  },
};
