import PortalVue from 'portal-vue';

export default {
  install(Vue) {
    Vue.use(PortalVue);
  },
};
