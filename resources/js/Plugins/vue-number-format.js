export default {
  install(Vue) {
    const currencyFormatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      currencyDisplay: 'code',
      minimumFractionDigits: 0,
    });

    Vue.prototype.$currencyFormatter = value => currencyFormatter.format(value);

    Vue.filter('currency', value => currencyFormatter.format(value));
  },
};
