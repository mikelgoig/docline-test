<?php

namespace App\Support\View;

use Inertia\Inertia;
use Inertia\Response;
use Spatie\ViewModels\ViewModel as BaseViewModel;

abstract class ViewModel extends BaseViewModel
{
    public function inertia(string $page): Response
    {
        return Inertia::render($page, $this);
    }

    protected function ignoredMethods(): array
    {
        return array_merge([
            'inertia',
        ], parent::ignoredMethods());
    }
}
