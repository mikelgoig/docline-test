<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/** @see \Tests\Domain\Models\ManagerTest */
class Manager extends Employee
{
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function departments(): BelongsToMany
    {
        return $this
            ->belongsToMany(Department::class, 'dept_manager', 'emp_no', 'dept_no')
            ->using(DeptManager::class)
            ->orderByPivot('from_date', 'asc')
            ->withPivot(['from_date', 'to_date']);
    }
}
