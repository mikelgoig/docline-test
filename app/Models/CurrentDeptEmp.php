<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/** @see \Tests\Domain\Models\CurrentDeptEmpTest */
class CurrentDeptEmp extends Pivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'current_dept_emp';

    /**
     * The name of the foreign key column.
     *
     * @var string
     */
    protected $foreignKey = 'dept_no';

    /**
     * The name of the "other key" column.
     *
     * @var string
     */
    protected $relatedKey = 'emp_no';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'from_date' => 'date:Y-m-d',
        'to_date' => 'date:Y-m-d',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getFromDateFormattedAttribute(): string
    {
        return $this->from_date->toFormattedDateString();
    }

    public function getToDateFormattedAttribute(): string
    {
        return $this->to_date->toFormattedDateString();
    }
}
