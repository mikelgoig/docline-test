<?php

namespace App\Models;

/** @see \Tests\Domain\Models\DeptManagerTest */
class DeptManager extends DeptEmp
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dept_manager';
}
