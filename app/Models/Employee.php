<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/** @see \Tests\Domain\Models\EmployeeTest */
class Employee extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'emp_no';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]|bool
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'birth_date' => 'date:Y-m-d',
        'hire_date' => 'date:Y-m-d',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    /** @param \DateTimeInterface|string $date */
    public function scopeOnRecordDate(Builder $query, $date): Builder
    {
        return $query->whereHas('records', function ($recordsQuery) use ($date) {
            $recordsQuery->onDate($date);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getBirthDateFormattedAttribute(): string
    {
        return $this->birth_date->format('Y-m-d');
    }

    public function getHireDateFormattedAttribute(): string
    {
        return $this->hire_date->format('Y-m-d');
    }

    public function getCurrentDepartmentAttribute(): ?Department
    {
        return $this->currentDepartments->first();
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function departments(): BelongsToMany
    {
        return $this
            ->belongsToMany(Department::class, 'dept_emp', 'emp_no', 'dept_no')
            ->using(DeptEmp::class)
            ->orderByPivot('from_date', 'asc')
            ->withPivot(['from_date', 'to_date']);
    }

    public function currentDepartments(): BelongsToMany
    {
        return $this
            ->belongsToMany(Department::class, 'current_dept_emp', 'emp_no', 'dept_no')
            ->using(CurrentDeptEmp::class)
            ->withPivot(['from_date', 'to_date']);
    }

    public function salaries(): HasMany
    {
        return $this->hasMany(Salary::class, 'emp_no', 'emp_no');
    }

    public function currentSalary(): HasOne
    {
        return $this->hasOne(CurrentSalary::class, 'emp_no', 'emp_no');
    }

    public function records(): HasMany
    {
        return $this->hasMany(EmployeeRecord::class, 'emp_no', 'emp_no');
    }
}
