<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/** @see \Tests\Domain\Models\DepartmentTest */
class Department extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'dept_no';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]|bool
     */
    protected $guarded = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function employees(): BelongsToMany
    {
        return $this
            ->belongsToMany(Employee::class, 'dept_emp', 'dept_no', 'emp_no')
            ->using(DeptEmp::class)
            ->orderByPivot('from_date', 'asc')
            ->withPivot(['from_date', 'to_date']);
    }

    public function managers(): BelongsToMany
    {
        return $this
            ->belongsToMany(Manager::class, 'dept_manager', 'dept_no', 'emp_no')
            ->using(DeptManager::class)
            ->orderByPivot('from_date', 'asc')
            ->withPivot(['from_date', 'to_date']);
    }
}
