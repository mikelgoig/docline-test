<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     *
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     *
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'request' => function () use ($request) {
                $route = $request->route();

                return [
                    'path' => $request->getPathInfo(),
                    'full_path' => $request->fullUrl(),
                    'params' => $request->all(),
                    'query' => $request->query(),
                    'route_name' => $route instanceof Route
                        ? $route->getName()
                        : '',
                ];
            },
        ]);
    }
}
