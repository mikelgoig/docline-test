<?php

namespace App\Http\ViewModels;

use App\Models\Employee;
use App\Support\View\ViewModel;

/** @see \Tests\App\ViewModels\EmployeeShowViewModelTest */
class EmployeeShowViewModel extends ViewModel
{
    private Employee $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function employee(): array
    {
        $currentDepartment = $this->employee->current_department;

        $currentSalary = $this->employee->currentSalary;

        return [
            'emp_no' => $this->employee->emp_no,
            'full_name' => $this->employee->full_name,
            'gender' => $this->employee->gender,
            'birth_date_formatted' => $this->employee->birth_date_formatted,
            'hire_date_formatted' => $this->employee->hire_date_formatted,

            'departments' => $this->employee->departments
                ->map(fn ($department) => [
                    'id' => $department->dept_no,
                    'name' => $department->dept_name,
                    'from_date' => $department->pivot->from_month,
                    'from_date_formatted' => $department->pivot->from_date_formatted,
                ])
                ->toArray(),

            'current_department' => $currentDepartment
                ? [
                    'name' => $currentDepartment->dept_name,
                    // @phpstan-ignore-next-line
                    'from_date_formatted' => $currentDepartment->pivot->from_date_formatted,
                ]
                : null,

            'current_salary' => $currentSalary->salary ?? null,
        ];
    }
}
