<?php

namespace App\Http\ViewModels;

use App\Http\Queries\EmployeeIndexQuery;
use App\Models\Manager;
use App\Support\View\ViewModel;
use Inertia\Inertia;
use Inertia\LazyProp;

/** @see \Tests\App\ViewModels\EmployeeIndexViewModelTest */
class EmployeeIndexViewModel extends ViewModel
{
    private EmployeeIndexQuery $employeeIndexQuery;

    public function __construct(EmployeeIndexQuery $employeeIndexQuery)
    {
        $this->employeeIndexQuery = $employeeIndexQuery;
    }

    public function employees(): callable
    {
        return fn () => $this->employeeIndexQuery
            ->paginate()
            ->transform(fn ($employee) => [
                'emp_no' => $employee->emp_no,
                'first_name' => $employee->first_name,
                'last_name' => $employee->last_name,
                'gender' => $employee->gender,
                'birth_date_formatted' => $employee->birth_date_formatted,
                'hire_date_formatted' => $employee->hire_date_formatted,
                'current_department' => ($currentDepartment = $employee->current_department)
                    ? $currentDepartment->dept_name
                    : null,
            ])
            ->toArray();
    }

    public function managers(): LazyProp
    {
        return Inertia::lazy(
            fn () => Manager::query()
                ->has('departments')
                ->orderBy('first_name')
                ->get()
                ->map(fn ($manager) => [
                    'emp_no' => $manager->emp_no,
                    'full_name' => $manager->full_name,
                ])
                ->toArray()
        );
    }
}
