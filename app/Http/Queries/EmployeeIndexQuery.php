<?php

namespace App\Http\Queries;

use App\Models\Employee;
use App\Support\QueryBuilder\Filters\FuzzyFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/** @see \Tests\App\Queries\EmployeeIndexQueryTest */
class EmployeeIndexQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Employee::query()
            ->with(['currentDepartments']);

        parent::__construct($query);

        $this
            ->defaultSort('emp_no')
            ->allowedFilters(
                AllowedFilter::custom('search', new FuzzyFilter(
                    'emp_no',
                    'birth_date',
                    'first_name',
                    'last_name',
                    'hire_date',
                )),

                AllowedFilter::exact('records.manager_emp_no'),

                AllowedFilter::scope('on_record_date'),
            )
            ->allowedSorts(
                'emp_no',
            );
    }
}
