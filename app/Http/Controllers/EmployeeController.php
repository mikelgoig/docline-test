<?php

namespace App\Http\Controllers;

use App\Http\Queries\EmployeeIndexQuery;
use App\Http\ViewModels\EmployeeIndexViewModel;
use App\Http\ViewModels\EmployeeShowViewModel;
use App\Models\Employee;
use Illuminate\Http\Request;
use Inertia\Response;

/** @see \Tests\App\Controllers\EmployeeControllerTest */
class EmployeeController
{
    /** Display a listing of the resource. */
    public function index(EmployeeIndexQuery $employeeIndexQuery): Response
    {
        return (new EmployeeIndexViewModel($employeeIndexQuery))
            ->inertia('Employee/EmployeeIndex');
    }

    /** Show the form for creating a new resource. */
    public function create(): void
    {
        //
    }

    /** Store a newly created resource in storage. */
    public function store(Request $request): void
    {
        //
    }

    /** Display the specified resource. */
    public function show(Employee $employee): Response
    {
        return (new EmployeeShowViewModel($employee))
            ->inertia('Employee/EmployeeShow');
    }

    /** Show the form for editing the specified resource. */
    public function edit(Employee $employee):void
    {
        //
    }

    /** Update the specified resource in storage. */
    public function update(Request $request, Employee $employee): void
    {
        //
    }

    /** Remove the specified resource from storage. */
    public function destroy(Employee $employee): void
    {
        //
    }
}
