<?php

namespace App\Providers;

use App\Support\Pagination\LengthAwarePaginator as MyLengthAwarePaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerLengthAwarePaginator();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function registerLengthAwarePaginator(): void
    {
        $this->app->bind(LengthAwarePaginator::class, function ($app, $values) {
            return new MyLengthAwarePaginator(...array_values($values));
        });
    }
}
