<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/', DashboardController::class)
        ->name('dashboard');

    Route::get('employees', [EmployeeController::class, 'index'])
        ->name('employee.index');

    Route::get('employees/{employee}', [EmployeeController::class, 'show'])
        ->name('employee.show');
});
