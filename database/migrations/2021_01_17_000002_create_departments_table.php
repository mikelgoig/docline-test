<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->char('dept_no', 4);
            $table->string('dept_name', 40);

            $table->primary('dept_no');
            $table->unique('dept_name');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('departments');
    }
}
