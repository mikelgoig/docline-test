<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSalariesLatestDateView extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS salaries_latest_date;');

        DB::statement('
            CREATE VIEW salaries_latest_date AS
                SELECT
                    emp_no,
                    MAX(from_date) AS from_date,
                    MAX(to_date) AS to_date
                FROM
                    salaries
                GROUP BY
                    emp_no;
        ');
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS salaries_latest_date;');
    }
}
