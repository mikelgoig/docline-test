<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDeptEmpLatestDateView extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS dept_emp_latest_date;');

        DB::statement('
            CREATE VIEW dept_emp_latest_date AS
                SELECT
                    emp_no,
                    MAX(from_date) AS from_date,
                    MAX(to_date) AS to_date
                FROM
                    dept_emp
                GROUP BY
                    emp_no;
        ');
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS dept_emp_latest_date;');
    }
}
