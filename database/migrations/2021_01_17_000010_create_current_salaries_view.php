<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCurrentSalariesView extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS current_salaries');

        DB::statement('
            CREATE VIEW current_salaries AS
                SELECT
                    l.emp_no,
                    salary,
                    l.from_date,
                    l.to_date
                FROM
                    salaries s
                INNER JOIN
                    salaries_latest_date l
                    ON s.emp_no=l.emp_no
                    AND s.from_date=l.from_date
                    AND l.to_date = s.to_date;
        ');
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS current_salaries');
    }
}
