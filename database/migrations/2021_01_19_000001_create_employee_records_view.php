<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateEmployeeRecordsView extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS employee_records');

        DB::statement('
            CREATE VIEW employee_records AS
                SELECT
                    employees.emp_no,
                    employees.first_name,
                    employees.last_name,
                    employees.gender,
                    employees.hire_date,
                    employees.birth_date,
                    dept_emp.from_date,
                    dept_emp.to_date,
                    dept_emp.dept_no,
                    departments.dept_name,
                    mananger.emp_no AS manager_emp_no,
                    mananger.first_name AS manager_first_name,
                    mananger.last_name AS manager_last_name,
                    dept_manager.from_date AS manager_from_date,
                    dept_manager.to_date AS manager_to_date
                FROM
                    employees
                LEFT JOIN
                    dept_emp
                    ON employees.emp_no = dept_emp.emp_no
                LEFT JOIN
                    departments
                    ON dept_emp.dept_no = departments.dept_no
                LEFT JOIN
                    dept_manager
                    ON departments.dept_no = dept_manager.dept_no
                LEFT JOIN
                    employees mananger
                    ON dept_manager.emp_no = mananger.emp_no;
            ');
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS employee_records');
    }
}
