<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->unsignedBigInteger('emp_no');
            $table->unsignedInteger('salary');
            $table->date('from_date');
            $table->date('to_date');

            $table
                ->foreign('emp_no')
                ->references('emp_no')
                ->on('employees')
                ->onDelete('cascade');

            $table->primary(['emp_no', 'from_date']);
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('salaries');
    }
}
