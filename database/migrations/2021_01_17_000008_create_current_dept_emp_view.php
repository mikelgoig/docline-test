<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCurrentDeptEmpView extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::statement('DROP VIEW IF EXISTS current_dept_emp');

        DB::statement('
            CREATE VIEW current_dept_emp AS
                SELECT
                    l.emp_no,
                    dept_no,
                    l.from_date,
                    l.to_date
                FROM
                    dept_emp d
                INNER JOIN
                    dept_emp_latest_date l
                    ON d.emp_no=l.emp_no
                    AND d.from_date=l.from_date
                    AND l.to_date = d.to_date;
        ');
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::statement('DROP VIEW IF EXISTS current_dept_emp');
    }
}
