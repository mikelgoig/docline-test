<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /** Run the database seeds. */
    public function run(): void
    {
        $test = new User();
        $test->name = 'Test';
        $test->email = 'test@docline.es';
        $test->password = Hash::make('secret');
        $test->save();
    }
}
