<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /** Run the database seeds. */
    public function run(): void
    {
        $queries = [];

        array_push($queries,
            file_get_contents(database_path('seeders/dumps/load_departments.dump')),
            file_get_contents(database_path('seeders/dumps/load_employees.dump')),
            file_get_contents(database_path('seeders/dumps/load_dept_emp.dump')),
            file_get_contents(database_path('seeders/dumps/load_dept_manager.dump')),
            file_get_contents(database_path('seeders/dumps/load_titles.dump')),
            file_get_contents(database_path('seeders/dumps/load_salaries1.dump')),
            file_get_contents(database_path('seeders/dumps/load_salaries2.dump')),
            file_get_contents(database_path('seeders/dumps/load_salaries3.dump')),
        );

        foreach ($queries as $query) {
            if (! $query) {
                return;
            }

            DB::unprepared($query);
        }
    }
}
