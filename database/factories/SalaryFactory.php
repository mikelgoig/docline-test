<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\Salary;
use Illuminate\Database\Eloquent\Factories\Factory;

class SalaryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Salary::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'emp_no' => Employee::factory(),
            'salary' => $this->faker->numberBetween(20000, 90000),
            'from_date' => $this->faker->date('Y-m-d'),
            'to_date' => today()->format('Y-m-d'),
        ];
    }
}
