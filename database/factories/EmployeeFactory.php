<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = $this->faker->randomElement(['M', 'F']);

        $firstName = $gender === 'M'
            ? $this->faker->firstNameMale
            : $this->faker->firstNameFemale;

        return [
            'birth_date' => $this->faker->date('Y-m-d', '2000-01-01'),
            'first_name' => $firstName,
            'last_name' => $this->faker->lastName,
            'gender' => $gender,
            'hire_date' => $this->faker->date(),
        ];
    }
}
