<?php

namespace Database\Factories;

use App\Models\Manager;

class ManagerFactory extends EmployeeFactory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Manager::class;
}
