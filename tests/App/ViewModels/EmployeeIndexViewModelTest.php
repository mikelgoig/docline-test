<?php

namespace Tests\App\ViewModels;

use App\Http\ViewModels\EmployeeIndexViewModel;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Manager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Http\ViewModels\EmployeeIndexViewModel */
class EmployeeIndexViewModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_get_employees_data()
    {
        Employee::factory()->create();

        $viewModel = app(EmployeeIndexViewModel::class);

        $employees = call_user_func($viewModel->employees());

        $this->assertEquals(
            [
                'emp_no',
                'first_name',
                'last_name',
                'gender',
                'birth_date_formatted',
                'hire_date_formatted',
                'current_department',
            ],
            array_keys($employees['data'][0])
        );
    }

    /** @test */
    public function it_can_get_managers_data()
    {
        Manager::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => '2020-01-01',
                'to_date' => '2020-12-31',
            ])
            ->create([
                'emp_no' => 1,
            ]);

        $viewModel = app(EmployeeIndexViewModel::class);

        $managers = call_user_func($viewModel->managers());

        $this->assertEquals(
            [
                'emp_no',
                'full_name',
            ],
            array_keys($managers[0])
        );

        $this->assertEquals(1, $managers[0]['emp_no']);
    }
}
