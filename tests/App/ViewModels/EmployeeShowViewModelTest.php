<?php

namespace Tests\App\ViewModels;

use App\Http\ViewModels\EmployeeShowViewModel;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Http\ViewModels\EmployeeShowViewModel */
class EmployeeShowViewModelTest extends TestCase
{
    use RefreshDatabase;

    private EmployeeShowViewModel $viewModel;

    protected function setUp(): void
    {
        parent::setUp();

        $employee = Employee::factory()->create();

        $this->viewModel = new EmployeeShowViewModel($employee);
    }

    /** @test */
    public function it_can_get_employee_data()
    {
        $employee = $this->viewModel->employee();

        $this->assertEquals(
            [
                'emp_no',
                'full_name',
                'gender',
                'birth_date_formatted',
                'hire_date_formatted',
                'departments',
                'current_department',
                'current_salary',
            ],
            array_keys($employee)
        );
    }
}
