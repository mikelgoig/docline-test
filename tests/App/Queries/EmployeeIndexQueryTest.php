<?php

namespace Tests\App\Queries;

use App\Http\Queries\EmployeeIndexQuery;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Manager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\QueryTestCase;

/** @see \App\Http\Queries\EmployeeIndexQuery */
class EmployeeIndexQueryTest extends QueryTestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_search_for_employees()
    {
        Employee::factory()->count(5)->create();

        $matchedEmployee = Employee::factory()->create([
            'first_name' => 'Some Big Fancy Employee Name',
        ]);

        $query = $this->createQuery(
            EmployeeIndexQuery::class,
            Arr::query(['filter' => ['search' => 'fancy employee']])
        );

        $employees = $query->get();

        $this->assertCount(1, $employees);
        $this->assertTrue($employees->contains($matchedEmployee));
    }

    /** @test */
    public function it_can_filter_by_manager()
    {
        Department::factory()
            ->hasAttached(Manager::factory()->state([
                'emp_no' => 1,
            ]), [
                'from_date' => '2000-01-01',
                'to_date' => '2000-12-31',
            ])
            ->hasAttached(Employee::factory()->state([
                'emp_no' => 2,
            ]), [
                'from_date' => '2000-01-01',
                'to_date' => '2000-12-31',
            ])
            ->create();

        $query = $this->createQuery(
            EmployeeIndexQuery::class,
            Arr::query(['filter' => [
                'records.manager_emp_no' => 1,
            ]])
        );

        $employees = $query->get();

        $this->assertCount(1, $employees);
        $this->assertTrue($employees->contains('emp_no', '2'));
    }

    /** @test */
    public function it_can_filter_by_manager_and_date()
    {
        Department::factory()
            ->hasAttached(Manager::factory()->state([
                'emp_no' => 1,
            ]), [
                'from_date' => '2000-01-01',
                'to_date' => '2000-12-31',
            ])
            ->hasAttached(Employee::factory()->state([
                'emp_no' => 2,
            ]), [
                'from_date' => '2000-11-01',
                'to_date' => '2000-12-31',
            ])
            ->create();

        // Fails

        $query = $this->createQuery(
          EmployeeIndexQuery::class,
          Arr::query(['filter' => [
              'records.manager_emp_no' => 1,
              'on_record_date' => '2000-01-01',
          ]])
        );

        $employees = $query->get();

        $this->assertCount(0, $employees);
        $this->assertFalse($employees->contains('emp_no', '2'));

        // Success

        $query = $this->createQuery(
            EmployeeIndexQuery::class,
            Arr::query(['filter' => [
                'records.manager_emp_no' => 1,
                'on_record_date' => '2000-11-01',
            ]])
        );

        $employees = $query->get();

        $this->assertCount(1, $employees);
        $this->assertTrue($employees->contains('emp_no', '2'));
    }
}
