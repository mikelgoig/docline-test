<?php

namespace Tests\App\Controllers;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Http\Controllers\EmployeeController */
class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    /*
    |--------------------------------------------------------------------------
    | Index
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function auth_user_can_see_a_listing_of_employees()
    {
        $this
            ->actingAs(User::factory()->create())
            ->get(route('employee.index'))
            ->assertSuccessful();
    }

    /*
    |--------------------------------------------------------------------------
    | Show
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function auth_user_can_see_a_specified_employee()
    {
        $this
            ->actingAs(User::factory()->create())
            ->get(route('employee.show', [
                'employee' => Employee::factory()->create()->emp_no,
            ]))
            ->assertSuccessful();
    }
}
