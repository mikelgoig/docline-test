<?php

namespace Tests;

use Spatie\QueryBuilder\QueryBuilder;

abstract class QueryTestCase extends TestCase
{
    protected function createQuery(
        string $queryBuilder,
        ?string $requestQuery = null
    ): QueryBuilder {
        if ($requestQuery) {
            $this->getJson("/test?{$requestQuery}");
        }

        return app($queryBuilder);
    }
}
