<?php

namespace Tests\Domain\Models;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Manager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Models\Department */
class DepartmentTest extends TestCase
{
    use RefreshDatabase;

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function it_belongs_to_many_employees()
    {
        $department = Department::factory()
            ->hasAttached(Employee::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Collection::class, $department->employees);
    }

    /** @test */
    public function it_belongs_to_many_managers()
    {
        $department = Department::factory()
            ->hasAttached(Manager::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Collection::class, $department->managers);
    }
}
