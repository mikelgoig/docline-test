<?php

namespace Tests\Domain\Models;

use App\Models\Department;
use App\Models\Manager;
use Illuminate\Database\Eloquent\Collection;

/** @see \App\Models\Employee */
class ManagerTest extends EmployeeTest
{
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function it_belongs_to_many_departments()
    {
        $manager = Manager::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Collection::class, $manager->departments);
    }
}
