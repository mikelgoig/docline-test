<?php

namespace Tests\Domain\Models;

use App\Models\CurrentSalary;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Salary;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Models\Employee */
class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /*
    |----------------------------------------------------------------------
    | Accessors
    |----------------------------------------------------------------------
    */

    /** @test */
    public function it_can_get_its_full_name_attribute()
    {
        $employee = Employee::factory()->make([
            'first_name' => 'Pepe',
            'last_name' => 'Grillo',
        ]);

        $this->assertEquals('Pepe Grillo', $employee->full_name);
    }

    /** @test */
    public function it_can_access_to_birth_date_formatted()
    {
        $employee = Employee::factory()->make([
            'birth_date' => Carbon::parse('1993-05-16'),
        ]);

        $this->assertEquals('1993-05-16', $employee->birth_date_formatted);
    }

    /** @test */
    public function it_can_access_to_hire_date_formatted()
    {
        $employee = Employee::factory()->make([
            'hire_date' => Carbon::parse('2020-01-01'),
        ]);

        $this->assertEquals('2020-01-01', $employee->hire_date_formatted);
    }

    /** @test */
    public function it_can_access_to_current_department()
    {
        $employee = Employee::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Department::class, $employee->current_department);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function it_belongs_to_many_departments()
    {
        $employee = Employee::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Collection::class, $employee->departments);
    }

    /** @test */
    public function it_belongs_to_many_current_departments()
    {
        $employee = Employee::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => today()->subDays(7),
                'to_date' => today(),
            ])
            ->create();

        $this->assertInstanceOf(Collection::class, $employee->currentDepartments);
    }

    /** @test */
    public function it_has_many_salaries()
    {
        $employee = Employee::factory()
            ->has(Salary::factory()->count(2))
            ->create();

        $this->assertInstanceOf(Collection::class, $employee->salaries);
    }

    /** @test */
    public function it_has_one_current_salary()
    {
        $employee = Employee::factory()
            ->has(Salary::factory()->count(2))
            ->create();

        $this->assertInstanceOf(CurrentSalary::class, $employee->currentSalary);
    }

    /** @test */
    public function it_has_many_records()
    {
        $employee = Employee::factory()->create();

        $this->assertInstanceOf(Collection::class, $employee->records);
    }
}
