<?php

namespace Tests\Domain\Models;

use App\Models\Employee;
use App\Models\Salary;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Models\CurrentSalary */
class CurrentSalaryTest extends TestCase
{
    use RefreshDatabase;

    /*
    |----------------------------------------------------------------------
    | Accessors
    |----------------------------------------------------------------------
    */

    /** @test */
    public function it_can_access_to_from_date_formatted()
    {
        $employee = Employee::factory()
            ->has(Salary::factory([
                'from_date' => '2020-01-01',
            ]))
            ->create();

        $this->assertEquals(
            'Jan 1, 2020',
            $employee->currentSalary->from_date_formatted
        );
    }

    /** @test */
    public function it_can_access_to_to_date_formatted()
    {
        $employee = Employee::factory()
            ->has(Salary::factory([
                'to_date' => '2020-01-01',
            ]))
            ->create();

        $this->assertEquals(
            'Jan 1, 2020',
            $employee->currentSalary->to_date_formatted
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /** @test */
    public function it_belongs_to_an_employee()
    {
        $employee = Employee::factory()
            ->has(Salary::factory([
                'to_date' => '2020-01-01',
            ]))
            ->create();

        $this->assertInstanceOf(Employee::class, $employee->currentSalary->employee);
    }
}
