<?php

namespace Tests\Domain\Models;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/** @see \App\Models\CurrentDeptEmp */
class CurrentDeptEmpTest extends TestCase
{
    use RefreshDatabase;

    /*
    |----------------------------------------------------------------------
    | Accessors
    |----------------------------------------------------------------------
    */

    /** @test */
    public function it_can_access_to_from_date_formatted()
    {
        $employee = Employee::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => '2020-01-01',
                'to_date' => '2020-01-02',
            ])
            ->create();

        $this->assertEquals(
            'Jan 1, 2020',
            $employee->current_department->pivot->from_date_formatted
        );
    }

    /** @test */
    public function it_can_access_to_to_date_formatted()
    {
        $employee = Employee::factory()
            ->hasAttached(Department::factory(), [
                'from_date' => '2020-01-01',
                'to_date' => '2020-01-02',
            ])
            ->create();

        $this->assertEquals(
            'Jan 2, 2020',
            $employee->current_department->pivot->to_date_formatted
        );
    }
}
