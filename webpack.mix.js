const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/js/app.js', 'public/js')

  .vue()

  .browserSync({
    files: 'resources/**/*',
    proxy: process.env.APP_URL,
    open: false,
  })

  .postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('postcss-nesting'),
    require('tailwindcss'),
    require('autoprefixer'),
  ])

  .sourceMaps(!mix.inProduction())

  .webpackConfig(require('./webpack.config'));

if (mix.inProduction()) {
  mix.version();
}
